import React from 'react';
import * as axios from "axios";

export class LoginForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: null
    }
  }

  doLogin = (e) => {
    e.preventDefault();
    const {username, password} = this.state
    const {history, setLoginData} = this.props;
    const formData = new FormData()
    formData.append('username', username)
    formData.append('password', password)
    axios.post('https://localhost:5001/login', formData,
      {headers: {'Content-Type': 'multipart/form-data'}})
      .then((response) => {
        setLoginData(response.data)
        history.push('/')
      })
      .catch((error) => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          this.setState({error: `Failed to login. Status code: ${error.response.status}. Server message: ${error.response.data}`})
        } else {
          this.setState({error: `Failed to login.`})
        }
        setTimeout(() => this.setState({username: '', password: '', error: null}), 1500)
      })
  }

  handleFieldChange = ({target}) => {
    const actualState = this.state
    this.setState({
      actualState,
      [target.getAttribute('name')]: target.value,
    })
  }

  handleCancel = (e) => {
    e.preventDefault()
    this.props.history.push('/')
  }

  render() {
    const {username, password, error} = this.state
    return (
      <>
        <div className="offset-3"><h1>Login</h1></div>
        <hr/>
        <form method='post' className="col-6 offset-3">
          <div className="form-group">
            <label>Username</label>
            <input name="username" onChange={this.handleFieldChange} value={username} type="text"
                   className="form-control"/>
          </div>
          <div className="form-group">
            <label>Password</label>
            <input name="password" onChange={this.handleFieldChange} value={password} type="password"
                   className="form-control"/>
          </div>
          <br/>
          {error && <>
            <div className='text-danger'>{error}</div>
            <br/></>}
          <button onClick={this.doLogin} className="btn btn-primary">Aceptar</button>
          &nbsp;
          <button onClick={this.handleCancel} className="btn">Cancelar</button>
        </form>
      </>
    )
  }
}
