import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:5000/api/v1',
})

export const login = async (username,password) => await api.post(`/users`, {username:username, password:password})
export const getAllMsgs = async () => api.get(`/messages`)

const apis = {
    login,
    getAllMsgs,
}

export default apis
