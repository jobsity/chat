import React from "react";

export class Message extends React.Component {

  render() {
    const { msg } = this.props
    return (
      <>
        <div className='bg-light d-flex justify-content-between align-items-center'>
          <b>{msg.nameOrigin}</b>
          <b>{new Date(msg.timestamp).toUTCString()}</b>
        </div>
        <div>
          {msg.content}
        </div>
        <br />
      </>
    )
  }
}
