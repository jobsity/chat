import React from 'react';
import './App.css';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {LoginForm} from "./LoginForm";
import {NotFound} from "./NotFound";
import {RoomComponent} from "./RoomComponent";

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userLogged: false,
      userId: null,
      userName: null,
      token: null
    }
  }

  setLoginData = (data) => {
    if (data) {
      this.setState({
        userLogged: true,
        userId: data.userId,
        userName: data.username,
        token: data.token
      })
    } else {
      this.setState({
        userLogged: false,
        userId: null,
        userName: null,
        token: null
      })
    }
  }

  render() {
    return (
      <div className="container">
        <BrowserRouter>
          <Switch>
            <Route path={'/login'} render={(props) => <LoginForm {...props} setLoginData={this.setLoginData}/>}/>
            <Route path={'/room'} render={(props) => {
              if (this.state.userLogged)
                return <RoomComponent {...this.state} {...props} />
              else
                return <Redirect to='/login'/>
            }}/>
            <Route path={'/'} render={() => <Redirect to={this.state.userLogged ? '/room' : '/login'}/>}/>
            <Route component={NotFound}/>
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
}

export default App;
