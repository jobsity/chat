import React from "react";

export function NotFound(props) {

  const goBack = () => props.history.goBack()

  return (
    <div className="NotFound">
      <h3>Page not found</h3>
      <div className='btn btn-primary' onClick={goBack}>{"<"}>Go Back</div>
    </div>
  )
}
