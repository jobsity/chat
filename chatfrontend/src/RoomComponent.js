import React from "react";
import * as axios from "axios";
import {Message} from "./Message";


export class RoomComponent extends React.Component {
  intervalHandler = null
  inProgress = false

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      newText: '',
      error: ''
    }
  }

  sendMessage = () => {
    const newMsg = {
      // timestamp: new Date().toISOString(), //esta fecha no coincide con la que viene en los mensajes, esta en utc
      idorigin: this.props.userId,
      nameorigin: this.props.userName,
      content: this.state.newText
    }
    axios.post('https://localhost:5001/api/v1/messages', newMsg,
      {headers: {'Authorization': `Bearer ${this.props.token}`}})
      .then(response => {
        this.getMessages(true)
      })
      .catch(response => {
        this.setState({
          error: response.message
        })
        setTimeout(() => this.setState({error: null}), 1500)
      })
  }

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      e.stopPropagation()
      e.preventDefault()
      this.sendMessage()
    }
  }

  handleTextChange = ({target}) => {
    this.setState({
      newText: target.value
    })
  }

  componentDidMount() {
    this.intervalHandler = setInterval(()=>this.getMessages(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.intervalHandler)
  }

  getMessages(clearEditor) {
    if (this.inProgress) {
      return
    }
    this.inProgress = true
    axios.get('https://localhost:5001/api/v1/messages', {headers: {'Authorization': `Bearer ${this.props.token}`}})
      .then((response) => {
        this.setState({
          messages: response.data,
          newText: clearEditor ? '' : this.state.newText
        })
        this.inProgress = false
      })
      .catch((error) => {
        this.setState({messages: [], error:error.response?error.response.data:''})
        this.inProgress = false
        if (error.response && error.response.status === 401)
          this.props.history.push('/login')
      })
  }

  render() {
    const {messages, newText, error} = this.state
    return (
      <>
        <div>
          <div>
            <div className="d-flex justify-content-between align-items-center">
              <h1>Chat Room</h1>
              <div>User: {this.props.userName}</div>
            </div>
            {messages.map(msg => (
              <div key={msg.id}>
                <Message msg={msg}/>
              </div>))}
          </div>

          <div className="align-self-end">
            <input type="text" onChange={this.handleTextChange} onKeyUp={this.handleKeyUp} value={newText}/>
            <button onClick={this.sendMessage}>Send</button>
          </div>
          {error && <div className="text-danger">Error! {error}</div>}
        </div>
      </>
    )
  }

}
