﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatBackend.Controllers;
using Data.model;
using Infrastructure.repositories;
using FluentAssertions;
using Infrastructure;
using Infrastructure.queue;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace ChatUnitTests.controllers
{
    [TestClass]
    public class MessagesControllerTests
    {
        private MessagesController _controller;
        private IMessagesRepository _fakeMsgRepo;
        private IQuotesRepository _fakeQuotesRepo;
        private IConfiguration _fakeConfig;
        private IQueueManager _fakeQueueManager;

        [TestInitialize]
        public void Initialize()
        {
            _fakeMsgRepo = Substitute.For<IMessagesRepository>();
            _fakeQuotesRepo = Substitute.For<IQuotesRepository>();
            
            _fakeConfig = Substitute.For<IConfiguration>();
            _fakeConfig.GetSection("Messages:Limit").Value.Returns("50");
            
            _fakeQueueManager = Substitute.For<IQueueManager>();
            
            _controller = new MessagesController(_fakeMsgRepo, _fakeConfig, _fakeQueueManager, _fakeQuotesRepo, NullLogger<MessagesController>.Instance);
        }
        
        #region GetMessages

        [TestMethod]
        public async Task GetMessages_NoQuotes_Success()
        {
            var messages = new List<Message>()
            {
                new Message() {Id = 1, Content = "test1", IdOrigin = 1, Timestamp = DateTime.Parse("2020-10-01T10:05:04")},
                new Message() {Id = 2, Content = "test2", IdOrigin = 2, Timestamp = DateTime.Parse("2020-10-01T08:02:25")}
            };
            _fakeMsgRepo.GetLatestMessages(50).Returns(messages);

            var response = await _controller.GetMessages();

            response.Should().NotBeNull();
            response.Should().BeOfType<OkObjectResult>();
            var msgList = (List<Message>) ((OkObjectResult) response).Value;
            Assert.AreEqual(msgList.Count, 2);
            // verify order by timestamp
            Assert.AreEqual(msgList[0], messages[1]);
            Assert.AreEqual(msgList[1], messages[0]);
        }

        [TestMethod]
        public async Task GetMessages_Quotes_Success()
        {
            var messages = new List<Message>()
            {
                new Message() {Id = 1, Content = "test1", IdOrigin = 1, Timestamp = DateTime.Parse("2020-10-02T10:05:04")},
                new Message() {Id = 2, Content = "test2", IdOrigin = 2, Timestamp = DateTime.Parse("2020-10-01T08:02:25")}
            };
            _fakeMsgRepo.GetLatestMessages(50).Returns(messages);

            var quotes = new List<Message>()
            {
                new Message() {Id = 3, Content = "quote is blah blah", IdOrigin = 0, Timestamp = DateTime.Parse("2020-10-01T10:05:04"), NameOrigin = "quotebot"}
            };
            _fakeQuotesRepo.GetLatestQuotes(50).Returns(quotes);

            var response = await _controller.GetMessages();

            response.Should().NotBeNull();
            response.Should().BeOfType<OkObjectResult>();
            var msgList = (List<Message>) ((OkObjectResult) response).Value;
            Assert.AreEqual(msgList.Count, 3);
            //verify order by timestamp
            Assert.AreEqual(msgList[0], messages[1]);
            Assert.AreEqual(msgList[1], quotes[0]);
            Assert.AreEqual(msgList[2], messages[0]);
        }

        [TestMethod]
        public async Task GetMessages_ThrowException_Returns500()
        {
            _fakeMsgRepo.GetLatestMessages(50).Returns<List<Message>>(x => throw new Exception());

            var response = await _controller.GetMessages();

            response.Should().BeOfType<ObjectResult>();
            ((ObjectResult) response).StatusCode.Should().Be(500);
        }

        #endregion

        #region AddMessage

        [TestMethod]
        public async Task AddMessage_Success_Returns201()
        {
            var msg = new Message {Id = 999, IdOrigin = 1, Content = "Message 1", Timestamp = DateTime.Parse("2020-05-13T08:09:10")};
            _fakeMsgRepo.AddMessage(msg).Returns(true);

            var response = await _controller.AddMessage(msg);

            response.Should().BeOfType<CreatedResult>();
            ((CreatedResult) response).StatusCode.Should().Be(201);
            ((CreatedResult) response).Location.Should().Be($"api/v1/messages/{msg.Id}");
        } 

        [TestMethod]
        public async Task AddMessage_StockQuote_Returns200()
        {
            var msg = new Message {IdOrigin = 1, Content = $"{Constants.CmdString}msft.us"};

            var response = await _controller.AddMessage(msg);

            response.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult) response).StatusCode.Should().Be(200);
            _fakeMsgRepo.DidNotReceiveWithAnyArgs();
            await _fakeQueueManager.Received(1).EnqueueCommand(msg);
        } 

        [TestMethod]
        public async Task AddMessage_Fail_Returns400()
        {
            var msg = new Message {Id = 999, IdOrigin = 1, Content = "Message 1", Timestamp = DateTime.Parse("2020-05-13T08:09:10")};
            _fakeMsgRepo.AddMessage(msg).Returns(false);

            var response = await _controller.AddMessage(msg);

            response.Should().BeOfType<BadRequestResult>();
            ((BadRequestResult) response).StatusCode.Should().Be(400);
        } 

        [TestMethod]
        public async Task AddMessage_ThrowException_Returns500()
        {
            var msg = new Message {Id = 999, IdOrigin = 1, Content = "Message 1", Timestamp = DateTime.Parse("2020-05-13T08:09:10")};
            _fakeMsgRepo.AddMessage(msg).Returns<bool>(x => throw new Exception());

            var response = await _controller.AddMessage(msg);

            response.Should().BeOfType<ObjectResult>();
            ((ObjectResult) response).StatusCode.Should().Be(500);
        }

        #endregion
    }
}