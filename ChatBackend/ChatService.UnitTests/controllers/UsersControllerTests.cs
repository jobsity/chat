﻿using System.Threading.Tasks;
using ChatBackend.Controllers;
using Data.model;
using FluentAssertions;
using Infrastructure.repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace ChatUnitTests.controllers
{
    [TestClass]
    public class UsersControllerTests
    {
        private UsersController _usersController;
        private IUsersRepository _mockUsersRepository;

        [TestInitialize]
        public void Initialize()
        {
            _mockUsersRepository = Substitute.For<IUsersRepository>();
            _usersController = new UsersController(_mockUsersRepository, NullLogger<UsersController>.Instance);
        }

        #region Login
        [TestMethod]
        public async Task Login_Success()
        {
            var user = new User() {Id = 999, Username = "Mock User", Password = "pwd"};
            _mockUsersRepository.Authenticate(Arg.Any<string>(), Arg.Any<string>()).Returns(user);

            var response = await _usersController.Login("someuser", "somepwd");
            
            response.Should().NotBeNull();
            response.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult) response).StatusCode.Should().Be(200);
        }

        [TestMethod]
        public async Task Login_Unauthorized()
        {
            _mockUsersRepository.Authenticate(Arg.Any<string>(), Arg.Any<string>()).Returns(Task.FromResult<User>(null));

            var response = await _usersController.Login("someuser", "somepwd");
            
            response.Should().NotBeNull();
            response.Should().BeOfType<UnauthorizedObjectResult>();
            ((UnauthorizedObjectResult) response).StatusCode.Should().Be(401);
        }
        #endregion
    }
}