﻿﻿using System;
using System.Threading.Tasks;
 using Data.model;
 using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
 using Infrastructure.repositories;
 using Microsoft.Extensions.Logging.Abstractions;

 namespace Infrastructure.UnitTests.repositories
{
    [TestClass]
    public class MessagesRepositoryTests
    {
        private IMessagesRepository _repository;
        
        [TestInitialize]
        public void Initialize()
        {
            _repository = new MessagesListRepository(NullLogger<MessagesListRepository>.Instance);
        }
        
        [TestMethod]
        public async Task GetLatestMessages_ReturnsList()
        {
            var result = await _repository.GetLatestMessages(50);
            result.Should().NotBeNull();
            result.Count.Should().Be(3);
        }

        [TestMethod]
        public async Task GetMessage_ValidId_ReturnsMessage()
        {
            var result = await _repository.GetMessage(1);
            result.Should().NotBeNull();
            result.Id.Should().Be(1);
        }

        [TestMethod]
        public async Task GetMessage_InvalidId_ReturnsNull()
        {
            var result = await _repository.GetMessage(999);
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task AddMessage_ReturnsOk()
        {
            var msg = new Message {Id = 999, IdOrigin = 1, Content = "Message 1", Timestamp = DateTime.Parse("2020-05-13T08:09:10")};
            var result = await _repository.AddMessage(msg);
            result.Should().Be(true);
            msg.Id.Should().Be(4);
        }
    }
}