﻿﻿using System;
using System.Threading.Tasks;
 using Data.model;
 using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
 using Infrastructure.repositories;
 using Microsoft.Extensions.Configuration;
 using Microsoft.Extensions.Logging.Abstractions;
 using NSubstitute;

 namespace Infrastructure.UnitTests.repositories
{
    [TestClass]
    public class UsersRepositoryTests
    {
        private IConfiguration _configuration;
        private IUsersRepository _repository;
        
        [TestInitialize]
        public void Initialize()
        {
            _configuration = Substitute.For<IConfiguration>();
            _repository = new UsersListRepository(_configuration, NullLogger<UsersListRepository>.Instance);
        }
        
        [TestMethod]
        public async Task GetLatestUsers_ReturnsList()
        {
            var result = await _repository.GetUsers();
            result.Should().NotBeNull();
            result.Count.Should().Be(3);
        }

        [TestMethod]
        public async Task GetMessage_ValidId_ReturnsMessage()
        {
            var result = await _repository.GetUser(1);
            result.Should().NotBeNull();
            result.Id.Should().Be(1);
        }

        [TestMethod]
        public async Task GetMessage_InvalidId_ReturnsNull()
        {
            var result = await _repository.GetUser(9999);
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task AddMessage_ReturnsOk()
        {
            var user = new User {Id = 999, Username = "testuser", Password = "testPwd", Email = "test@test.com"};
            var result = await _repository.AddUser(user);
            result.Should().Be(true);
        }
    }
}