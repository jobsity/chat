﻿// using System;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Microsoft.VisualStudio.TestTools.UnitTesting;
// using FluentAssertions;
// using Infrastructure.model;
// using Infrastructure.repositories;
// using Microsoft.Extensions.Logging;
// using Microsoft.Extensions.Logging.Abstractions;
// using NSubstitute;
//
// namespace Infrastructure.UnitTests.repositories
// {
//     [TestClass]
//     public class MessagesDbRepositoryTests
//     {
//         private IMessagesRepository _repository;
//         private ChatDbContextFactory _dbContextFactoryMock;
//         
//         [TestInitialize]
//         public void Initialize()
//         {
//             //TODO fix the DbContext mock, then restore tests
//             // var dbContextMock = Substitute.For<ChatDbContext>();
//             // dbContextMock.Messages.Returns(DbContextMock.GetMockDbSet<Message>(new List<Message>()
//             // {
//             //     new Message() {Id = 1, IdOrigin = 1, NameOrigin = "User 1", Timestamp = DateTime.Parse("2020-07-01 10:02:53"), Content = "Message 1"},
//             //     new Message() {Id = 2, IdOrigin = 2, NameOrigin = "User 2", Timestamp = DateTime.Parse("2020-07-01 10:01:40"), Content = "Message 2"},
//             //     new Message() {Id = 3, IdOrigin = 1, NameOrigin = "User 1", Timestamp = DateTime.Parse("2020-07-01 11:00:12"), Content = "Message 3"}
//             // }));
//             // _dbContextFactoryMock = Substitute.For<ChatDbContextFactory>();
//             // _dbContextFactoryMock.GetContext().Returns(dbContextMock);
//             // _repository = new MessagesDbRepository(NullLogger<MessagesDbRepository>.Instance, _dbContextFactoryMock);
//         }
//         
//         [TestMethod]
//         public async Task GetLatestMessages_ReturnsList()
//         {
//             // var result = await _repository.GetLatestMessages(50);
//             // result.Should().NotBeNull();
//             // result.Count.Should().Be(3);
//         }
//
//         [TestMethod]
//         public async Task GetMessage_ValidId_ReturnsMessage()
//         {
//             // var result = await _repository.GetMessage(1);
//             // result.Should().NotBeNull();
//             // result.Id.Should().Be(1);
//         }
//
//         [TestMethod]
//         public async Task GetMessage_InvalidId_ReturnsNull()
//         {
//             // var result = await _repository.GetMessage(999);
//             // result.Should().BeNull();
//         }
//
//         [TestMethod]
//         public async Task AddMessage_ReturnsOk()
//         {
//             // var msg = new Message {Id = 999, IdOrigin = 1, Content = "Message 1", Timestamp = DateTime.Parse("2020-05-13T08:09:10")};
//             // var result = await _repository.AddMessage(msg);
//             // result.Should().Be(true);
//             // msg.Id.Should().Be(4);
//         }
//     }
// }