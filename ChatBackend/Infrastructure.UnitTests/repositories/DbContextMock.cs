﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NSubstitute;

namespace Infrastructure.UnitTests.repositories
{
    public static class DbContextMock
    {
        public static DbSet<T> GetMockDbSet<T>(List<T> data) where T : class
        {
            var queryable = data.AsQueryable<T>();

            var dbSet = Substitute.ForPartsOf<DbSet<T>>();
            // dbSet.AsQueryable().Provider.Returns(queryable.Provider);
            // dbSet.As<IQueryable<T>>().Expression.Returns(queryable.Expression);
            // dbSet.As<IQueryable<T>>().ElementType.Returns(queryable.ElementType);
            // dbSet.As<IQueryable<T>>().GetEnumerator().Returns(queryable.GetEnumerator());
            dbSet.When(x => x.Add(Arg.Any<T>())).Do(x=>data.Add(x.Arg<T>()));

            return dbSet;
        }
    }
}