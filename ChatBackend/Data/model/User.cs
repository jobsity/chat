﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Data.model
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        [NotMapped]
        public string Token { get; set; }
    }
}