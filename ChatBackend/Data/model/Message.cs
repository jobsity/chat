﻿using System;

namespace Data.model
{
    public class Message
    {
        public int Id { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public int IdOrigin { get; set; }
        public string NameOrigin { get; set; }
        public string Content { get; set; }
    }
}