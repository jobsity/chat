﻿namespace Data.model
{
    public class StockQuote
    {
        public string Symbol { get; set; }
        // if the symbol is wrong or there is no data, all following will contain 'N/D'
        public string Date { get; set; }
        public string Time { get; set; }
        public string Open { get; set; }
        public string High { get; set; }
        public string Low { get; set; }
        public string Close { get; set; }
        public string Volume { get; set; }
    }
}