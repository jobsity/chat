﻿using Data.model;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class ChatDbContext: DbContext
    {

        public ChatDbContext(): base()
        {
        }

        public ChatDbContext(DbContextOptions<ChatDbContext> options) : base(options)
        {
        }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
            // optionsBuilder.UseSqlServer("Server=(localdb)\\chatdb;Database=Chat;Trusted_Connection=True");
        // }

        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}