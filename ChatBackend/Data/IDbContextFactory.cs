﻿namespace Data
{
    public interface IDbContextFactory
    {
        public ChatDbContext GetContext();
    }
}