﻿namespace Data
{
    public class ChatDbContextFactory : IDbContextFactory
    {
        public ChatDbContext GetContext()
        {
            return new ChatDbContext();
        }
    }
}