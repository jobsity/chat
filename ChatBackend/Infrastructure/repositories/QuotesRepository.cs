﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.model;

namespace Infrastructure.repositories
{
    public class QuotesRepository: IQuotesRepository
    {
        private readonly List<Message> _quoteMessages;

        public QuotesRepository()
        {
            _quoteMessages = new List<Message>();
        }
        
        public IEnumerable<Message> GetLatestQuotes(int msgLimit)
        {
            return _quoteMessages.OrderByDescending(m => m.Timestamp).Take(msgLimit).ToList();
        }

        public void AddQuote(StockQuote quote)
        {
            _quoteMessages.Add(new Message()
            {
                NameOrigin = "quotebot", 
                Timestamp = DateTime.UtcNow, 
                IdOrigin = 0, 
                Content = $"{quote.Symbol} quote is ${quote.Close} per share"
            });
        }
    }
}