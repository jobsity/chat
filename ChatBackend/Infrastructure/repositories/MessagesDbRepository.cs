﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Data.model;
using Infrastructure.utils;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;

namespace Infrastructure.repositories
{
    public class MessagesDbRepository : IMessagesRepository
    {
        private readonly ILogger<MessagesDbRepository> _logger;
        private readonly IDbContextFactory _dbContextFactory;

        public MessagesDbRepository(ILogger<MessagesDbRepository> logger, IDbContextFactory dbContextFactory)
        {
            _logger = logger;
            _dbContextFactory = dbContextFactory;
        }
        
        public async Task<bool> AddMessage(Message msg)
        {
            using (var db = _dbContextFactory.GetContext())
            {
                msg.Timestamp = DateTime.UtcNow; //always take the server datatime to avoid ordering problems
                await db.Messages.AddAsync(msg);
                var numChanges = await db.SaveChangesAsync();
                return numChanges == 1;
            }
        }

        public async Task<List<Message>> GetLatestMessages(int msgLimit)
        {
            using(var db = _dbContextFactory.GetContext())
            {
                return await db.Messages.OrderByDescending(m=>m.Timestamp).Take(msgLimit).ToListAsync();
            }
        }

        public async Task<Message> GetMessage(int id)
        {
            using(var db = _dbContextFactory.GetContext())
            {
                return await db.Messages.SingleAsync(m => m.Id == id);
            }
        }
    }
}