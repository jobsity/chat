﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.model;
using Microsoft.Extensions.Logging;

namespace Infrastructure.repositories
{
    public class MessagesListRepository: IMessagesRepository
    {
        private readonly ILogger<MessagesListRepository> _logger;
        private List<Message> _messages = new List<Message>();

        public MessagesListRepository(ILogger<MessagesListRepository> logger)
        {
            _logger = logger;
            _messages.Add(new Message() {Id = 1, IdOrigin = 1, NameOrigin = "User 1", Timestamp = DateTime.UtcNow, Content = "Message 1"});
            _messages.Add(new Message() {Id = 2, IdOrigin = 1, NameOrigin = "User 1", Timestamp = DateTime.UtcNow, Content = "Message 2"});
            _messages.Add(new Message() {Id = 3, IdOrigin = 2, NameOrigin = "User 2", Timestamp = DateTime.UtcNow, Content = "Message 3"});
        }

        public Task<List<Message>> GetLatestMessages(int msgLimit) 
        {
            return Task.FromResult(_messages);
        }

        public Task<Message> GetMessage(int id)
        {
            return Task.FromResult(_messages.Find((msg) => msg.Id == id));
        }

        public Task<bool> AddMessage(Message msg)
        {
            try
            {
                var newId = _messages.Max(m => m.Id) + 1;
                msg.Id = newId;
                _messages.Add(msg);
                return Task.FromResult(true);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error adding a message: {e.Message}");
                return Task.FromResult(false);
            }
        }
    }
}