﻿using System.Collections.Generic;
using System.Security;
using System.Threading.Tasks;
using Data.model;

namespace Infrastructure.repositories
{
    public interface IUsersRepository
    {
        public Task<User> Authenticate(string username, string password);
        Task<List<User>> GetUsers();
        Task<User> GetUser(int id);
        Task<bool> AddUser(User user);
    }
}