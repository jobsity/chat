﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.model;
using Microsoft.Extensions.Configuration;
using Infrastructure.utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Infrastructure.repositories
{
    public class UsersListRepository : IUsersRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<UsersListRepository> _logger;

        private readonly List<User> _users = new List<User>();

        public UsersListRepository(IConfiguration configuration, ILogger<UsersListRepository> logger)
        {
            _configuration = configuration;
            _logger = logger;
            _users.Add(new User() { Id = 0, Username = "quotebot", Password = "", Email = "" });
            _users.Add(new User() { Id = 1, Username = "admin", Password = "admin123", Email = "admin@administators.com" });
            _users.Add(new User() { Id = 2, Username = "admin2", Password = "admin123", Email = "admin2@administators.com" });
        }

        public Task<User> Authenticate(string username, string password)
        {
            var user = _users.Find(u => u.Id > 0 && u.Username == username && u.Password == password);
            if (user == null) return Task.FromResult<User>(null);

            user.Token = Utils.GenerateJwtToken(user.Id, user.Username, _configuration);
            return Task.FromResult(user);
        }

        public Task<List<User>> GetUsers()
        {
            return Task.FromResult(_users);
        }

        public Task<User> GetUser(int id)
        {
            return Task.FromResult(_users.Find(u => u.Id == id));
        }

        public Task<bool> AddUser(User user)
        {
            var result = false;
            try
            {
                _users.Add(user);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }

            return Task.FromResult(result);
        }
    }
}