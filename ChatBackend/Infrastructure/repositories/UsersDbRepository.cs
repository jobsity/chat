﻿using System;
using System.Collections.Generic;
using Infrastructure.utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Data.model;
using Infrastructure.extensions;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;

namespace Infrastructure.repositories
{
    public class UsersDbRepository : IUsersRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IDbContextFactory _dbContextFactory;
        private readonly ILogger<UsersDbRepository> _logger;

        public UsersDbRepository(IConfiguration configuration, 
            IDbContextFactory dbContextFactory, 
            ILogger<UsersDbRepository> logger)
        {
            _configuration = configuration;
            _dbContextFactory = dbContextFactory;
            _logger = logger;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            using (var db = _dbContextFactory.GetContext())
            {
                try
                {
                    var hashedPwd = password.Sha256();
                    var user = await db.Users.SingleAsync(u => u.Id > 1 && u.Username == username && u.Password == hashedPwd);
                    if (user == null) return null;

                    user.Token = Utils.GenerateJwtToken(user.Id, user.Username, _configuration);
                    return user;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Error when authenticating user: {ex.Message}");
                    return null;
                }
            }
        }

        public async Task<List<User>> GetUsers()
        {
            using(var db = _dbContextFactory.GetContext())
            {
                return await db.Users.ToListAsync();
            }
        }

        public async Task<User> GetUser(int id)
        {
            using(var db = _dbContextFactory.GetContext())
            {
                return await db.Users.SingleAsync(m => m.Id == id);
            }
        }

        public async Task<bool> AddUser(User user)
        {
            using (var db = _dbContextFactory.GetContext())
            {
                user.Password = user.Password.Sha256();
                await db.Users.AddAsync(user);
                var numChanges = await db.SaveChangesAsync();
                return numChanges == 1;
            }        
        }

        public void ResetUserDb()
        {
            using (var db = _dbContextFactory.GetContext())
            {
                db.Users.RemoveRange(db.Users);
                db.SaveChanges();
                db.Users.Add(new User() {Id = 999, Username = "quotebot", Password = "qbot".Sha256()});
                db.Users.Add(new User() {Id = 1, Username = "user1", Password = "user123".Sha256()}); 
                db.Users.Add(new User() {Id = 2, Username = "user2", Password = "user345".Sha256()});
                db.SaveChanges();
            }
        }


    }
}
