﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.model;

namespace Infrastructure.repositories
{
    public interface IMessagesRepository
    {
        Task<List<Message>> GetLatestMessages(int msgLimit);
        Task<Message> GetMessage(int id);
        Task<bool> AddMessage(Message msg);
    }
}