﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.model;

namespace Infrastructure.repositories
{
    public interface IQuotesRepository
    {
        IEnumerable<Message> GetLatestQuotes(int msgLimit);
        void AddQuote(StockQuote quote);
    }
}