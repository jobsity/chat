﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Data.model;
using Infrastructure.configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Infrastructure.queue
{
    public class QueueManager : IQueueManager, IDisposable
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<QueueManager> _logger;
        private IConnection _connection;
        private QueueConfiguration _queueConfiguration;

        public QueueManager(IConfiguration configuration, ILogger<QueueManager> logger)
        {
            _configuration = configuration;
            _logger = logger;
            InitializeQueues();
        }

        private void InitializeQueues()
        {
            _queueConfiguration = QueueConfiguration.Create(_configuration);
            var factory = new ConnectionFactory()
            {
                HostName = _queueConfiguration.HostName,
                UserName = _queueConfiguration.Username,
                Password = _queueConfiguration.Password,
                DispatchConsumersAsync = true
            };
            _connection = factory.CreateConnection();
        }

        public Task EnqueueQuote(StockQuote quote)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: _queueConfiguration.QuotesQueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(quote));

                channel.BasicPublish(exchange: _queueConfiguration.ExchangeName,
                    routingKey: _queueConfiguration.QuotesQueueName, //use the name of the queue as key, used only in case of default exchange
                    basicProperties: null,
                    body: body);
            }

            return Task.CompletedTask;
        }

        public Task<List<StockQuote>> DequeueQuotes(int maxMessages=5)
        {
            var result = new List<StockQuote>();

            using (var channel = _connection.CreateModel())
            {
                //can't bind a queue to the default ("") exchange because it is implicitly bound
                if (_queueConfiguration.ExchangeName != "")
                {
                    channel.QueueBind(queue: _queueConfiguration.QuotesQueueName,
                        exchange: _queueConfiguration.ExchangeName,
                        routingKey: _queueConfiguration.CommandKey);
                }
                channel.QueueDeclare(queue: _queueConfiguration.QuotesQueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                while (channel.MessageCount(_queueConfiguration.QuotesQueueName) > 0 && result.Count <= maxMessages)
                {
                    var rabbitMsg = channel.BasicGet(_queueConfiguration.QuotesQueueName, false);
                    if (rabbitMsg == null) break;
                    var body = rabbitMsg.Body.ToArray();
                    var message = JsonSerializer.Deserialize<StockQuote>(Encoding.UTF8.GetString(body));
                    _logger.LogDebug("Received message: {}", message);
                    result.Add(message);
                    channel.BasicAck(rabbitMsg.DeliveryTag, false);
                };
            }

            return Task.FromResult(result);
        }

        public Task EnqueueCommand(Message msg)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: _queueConfiguration.CommandQueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                //enqueue the whole message in case we want to retrieve the origin id or the timestamp of creation or anything else
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(msg));

                channel.BasicPublish(exchange: _queueConfiguration.ExchangeName,
                    routingKey: _queueConfiguration.CommandKey,
                    basicProperties: null,
                    body: body);
            }

            return Task.CompletedTask;
        }

        public Task<List<Message>> DequeueCommands(int maxMessages = 5)
        {
            var result = new List<Message>();

            using (var channel = _connection.CreateModel())
            {
                //can't bind a queue to the default ("") exchange because it is implicitly bound
                if (_queueConfiguration.ExchangeName != "")
                {
                    channel.QueueBind(queue: _queueConfiguration.CommandQueueName,
                        exchange: _queueConfiguration.ExchangeName,
                        routingKey: _queueConfiguration.CommandKey);
                }

                while (channel.MessageCount(_queueConfiguration.CommandQueueName) > 0 && result.Count <= maxMessages)
                {
                    var rabbitMsg = channel.BasicGet(_queueConfiguration.CommandQueueName, false);
                    if (rabbitMsg == null) break;
                    var body = rabbitMsg.Body.ToArray();
                    var message = JsonSerializer.Deserialize<Message>(Encoding.UTF8.GetString(body));
                    _logger.LogDebug("Received message: {}", message);
                    result.Add(message);
                    channel.BasicAck(rabbitMsg.DeliveryTag, false);
                };
            }

            return Task.FromResult(result);
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}