﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.model;

namespace Infrastructure.queue
{
    public interface IQueueManager
    {
        Task EnqueueCommand(Message msg);
        Task<List<Message>> DequeueCommands(int maxMessages=5);
        
        Task EnqueueQuote(StockQuote quote);
        Task<List<StockQuote>> DequeueQuotes(int maxMessages=5);
    }
}