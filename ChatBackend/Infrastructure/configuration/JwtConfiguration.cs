﻿using System;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.configuration
{
    public class JwtConfiguration
    {
        private const string JwtSection = "Jwt";
        
        public string Key { get; set; }
        public int ExpirationMinutes { get; set; }

        public JwtConfiguration()
        {
            ExpirationMinutes = 15;
        }

        public static JwtConfiguration Create(IConfiguration configuration)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));
            var jwtConfiguration = configuration.GetSection(JwtSection).Get<JwtConfiguration>();
            return jwtConfiguration ?? new JwtConfiguration();
        }

    }
}