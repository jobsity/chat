﻿using System;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.configuration
{
    public class QueueConfiguration
    {
        private const string QueueSection = "Queue";

        public string HostName { get; set; }
        public string CommandQueueName { get; set; }
        public string QuotesQueueName { get; set; }
        public string ExchangeName { get; set; }
        public string CommandKey { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public QueueConfiguration()
        {
            HostName = "localhost";
            CommandQueueName = "cmd-queue";
            QuotesQueueName = "quotes-queue";
            Username = "guest";
            Password = "guest";
            ExchangeName = "";
            CommandKey = "";
        }

        public static QueueConfiguration Create(IConfiguration configuration)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));
            var queueConfiguration = configuration.GetSection(QueueSection).Get<QueueConfiguration>();
            return queueConfiguration ?? new QueueConfiguration();
        }
    }
}