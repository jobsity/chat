﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Data.model;
using Infrastructure;
using Infrastructure.queue;
using Infrastructure.repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBackend.Controllers
{
    [Route("/api/v1/[controller]")]
    [Authorize]
    public class MessagesController : ControllerBase
    {
        private readonly IMessagesRepository _messagesRepository;
        private readonly IConfiguration _configuration;
        private readonly IQueueManager _queueManager;
        private readonly IQuotesRepository _quotesRepository;
        private readonly ILogger<MessagesController> _logger;

        public MessagesController(IMessagesRepository messagesRepository,
            IConfiguration configuration,
            IQueueManager queueManager,
            IQuotesRepository quotesRepository,
            ILogger<MessagesController> logger)
        {
            _messagesRepository = messagesRepository;
            _configuration = configuration;
            _queueManager = queueManager;
            _quotesRepository = quotesRepository;
            _logger = logger;
        }

        [SwaggerOperation(Summary = "Gets a list of last 50 messages")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Messages retrieved")]
        [HttpGet]
        public async Task<IActionResult> GetMessages()
        {
            try
            {
                var msgLimit = _configuration.GetValue("Messages:Limit", 50);
                var messages = await _messagesRepository.GetLatestMessages(msgLimit);
                var stockMessages = _quotesRepository.GetLatestQuotes(msgLimit);
                messages.AddRange(stockMessages);

                return Ok(messages.OrderBy(msg => msg.Timestamp).TakeLast(msgLimit).ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving messages: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, "Could not retrieve latest messages");
            }
        }

        [SwaggerOperation(Summary = "Gets a single message")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMessage(int id)
        {
            try
            {
                var result = await _messagesRepository.GetMessage(id);

                if (result == null)
                {
                    return NotFound($"Message with id {id} could not be found.");
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving message: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, $"Could not retrieve message with id {id}");
            }
        }

        [SwaggerOperation(Summary = "Adds a message to the chat room",
            Description = "Supports commands to get stock quotes with format '/stock=[Symbol]'")]
        [SwaggerResponse((int)HttpStatusCode.Created, "Message added to the chat room", typeof(Message))]
        [SwaggerResponse((int)HttpStatusCode.OK, "Stock quote command received")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Wrong parameters")]
        [HttpPost]
        public async Task<IActionResult> AddMessage([FromBody] Message msg)
        {
            try
            {
                if (msg.Content.StartsWith(Constants.CmdString))
                {
                    await _queueManager.EnqueueCommand(msg);
                    return Ok("Stock query queued.");
                }

                var added = await _messagesRepository.AddMessage(msg);
                if (added) return Created($"api/v1/messages/{msg.Id}", msg);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error adding message: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, $"Could not add message.");
            }

            return BadRequest();
        }
    }
}