﻿using System;
using System.Net;
using System.Threading.Tasks;
using Data.model;
using Infrastructure.repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBackend.Controllers
{
    [Route("/api/v1/users")]
    [Authorize]
    public class UsersController: ControllerBase
    {
        private readonly IUsersRepository _usersRepository;
        private readonly ILogger<UsersController> _logger;

        public UsersController(IUsersRepository usersRepository, ILogger<UsersController> logger)
        {
            _usersRepository = usersRepository;
            _logger = logger;
        }

        [SwaggerOperation(Summary = "Login to the system")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Login successful")]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized, "Wrong credentials")]
        [AllowAnonymous]
        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password)
        {
            var user = await _usersRepository.Authenticate(username, password);
            if (user != null)
            {
                return Ok(new {token=user.Token, username=user.Username, userId=user.Id});
            }

            return Unauthorized("Login incorrect. Please try again");
        }
        
        [SwaggerOperation(Summary = "Gets the list of known users")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Users retrieved")]
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var users = await _usersRepository.GetUsers();
                return Ok(users);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving users: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, "Could not retrieve users");
            }
        }

        [SwaggerOperation(Summary = "Adds a user to the chat room")]
        [SwaggerResponse((int)HttpStatusCode.Created, "User added", typeof(User))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Wrong parameters")]
        [HttpPost("users")]
        public async Task<IActionResult> AddUser([FromBody] User user)
        {
            try
            {
                var added = await _usersRepository.AddUser(user);
                if (added) return Created($"api/v1/users/{user.Id}", user);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error adding user: {ex.Message}");
                _logger.LogError($"Internal exception: {ex.InnerException?.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, $"Could not add user.");
            }

            return BadRequest();
        }

        [SwaggerOperation(Summary = "Gets a single user")]
        [SwaggerResponse((int)HttpStatusCode.OK, "User found", typeof(User))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, "User not found")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var result = await _usersRepository.GetUser(id);

                if (result == null)
                {
                    return NotFound($"Message with id {id} could not be found.");
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving user: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, $"Could not retrieve user with id {id}");
            }
        }

    }
}