using System.Text;
using ChatBackend.BackgroundProcessors;
using Data;
using Infrastructure.queue;
using Infrastructure.repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace ChatBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var cnxString = Configuration.GetConnectionString("ChatConnection");
            services.AddDbContext<ChatDbContext>(options => options.UseSqlServer (cnxString));
            //factory to allow for easy testing
            services.AddSingleton<IDbContextFactory, ChatDbContextFactory>();
            //our services
            services.AddSingleton<IMessagesRepository, MessagesDbRepository>();
            services.AddSingleton<IUsersRepository, UsersDbRepository>();
            services.AddSingleton<IQuotesRepository, QuotesRepository>();
            services.AddSingleton<IQueueManager, QueueManager>();
            //background services to process queues
            services.AddHostedService<CommandsProcessor>();
            services.AddHostedService<QuotesProcessor>();

            //swagger support
            AddSwaggerSupport(services);

            //use jwt token bearer authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);
            services.AddCors();
            services.AddControllers();
        }

        private static void AddSwaggerSupport(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Title = "ChatRoom demo app",
                    Version = "v1",
                    Description = "Demo app for Jobsity",
                    Contact = new OpenApiContact()
                    {
                        Email = "ernestocullen@gmail.com",
                        Name = "Ernesto Cullen"
                    }
                });
                options.EnableAnnotations();
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Bearer",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter your token below"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement  
                {  
                    {  
                        new OpenApiSecurityScheme  
                        {  
                            Reference = new OpenApiReference  
                            {  
                                Type = ReferenceType.SecurityScheme,  
                                Id = "Bearer"  
                            }  
                        },  
                        new string[] {}  
                    }  
                });  
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //allow access from anywhere for the demo
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = "";
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "ChatRoom v1");
            });
            
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}