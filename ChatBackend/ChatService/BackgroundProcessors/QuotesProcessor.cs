﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.queue;
using Infrastructure.repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ChatBackend.BackgroundProcessors
{
    public class QuotesProcessor: BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<QuotesProcessor> _logger;
        private readonly IQueueManager _queueManager;
        private readonly IQuotesRepository _quotesRepository;

        public QuotesProcessor(IConfiguration configuration, 
            ILogger<QuotesProcessor> logger,
            IQueueManager queueManager,
            IQuotesRepository quotesRepository)
        {
            _configuration = configuration;
            _logger = logger;
            _queueManager = queueManager;
            _quotesRepository = quotesRepository;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    // Dequeue quotes
                    var quotes = await _queueManager.DequeueQuotes();
                    foreach (var quote in quotes)
                    {
                        _quotesRepository.AddQuote(quote);
                    }
                }
                catch (TaskCanceledException ex)
                {
                    var reason = ex.CancellationToken == stoppingToken
                        ? "Service is stopping."
                        : "Triggered by an unknown source.";
                    _logger.LogInformation(ex, $"Operation cancelled. {reason}");
                }
                catch (OperationCanceledException ex)
                {
                    var reason = ex.CancellationToken == stoppingToken
                        ? "Service is stopping."
                        : "Triggered by an unknown source.";
                    _logger.LogInformation(ex, $"Operation cancelled. {reason}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Unexpected exception while processing command");
                }

                await Task.Delay(_configuration.GetValue("CommandProcessingMillis", 5000), stoppingToken);
            }
        }
        
    }
    
    
}