﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CsvHelper;
using Data.model;
using Infrastructure;
using Infrastructure.queue;
using Microsoft.Extensions.Logging;

namespace ChatBackend.BackgroundProcessors
{
    public class QuoteTask
    {
        private readonly Message _cmdMessage;
        private readonly IQueueManager _queueManager;
        private readonly ILogger<QuoteTask> _logger;

        public QuoteTask(Message cmdMessage, IQueueManager queueManager)
        {
            _cmdMessage = cmdMessage;
            _queueManager = queueManager;
            _logger = new LoggerFactory().CreateLogger<QuoteTask>();
        }

        public async Task ResolveQuote()
        {
            var symbol = _cmdMessage.Content.Substring(Constants.CmdString.Length);
            if (string.IsNullOrWhiteSpace(symbol))
                throw new ArgumentException("Missing stock symbol");

            var quote = await GetStockQuote(symbol);
            if (quote == null)
                throw new ApplicationException("Stock quote could not be parsed");
            //queue quote
            await _queueManager.EnqueueQuote(quote);
        }

        private async Task<StockQuote> GetStockQuote(string symbol)
        {
            //connects to the api to get the quote
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync($@"https://stooq.com/q/l/?s={symbol}&f=sd2t2ohlcv&h&e=csv");
            response.EnsureSuccessStatusCode();
            var csvString = response.Content.ReadAsStringAsync().Result;
            using var reader = new StringReader(csvString);
            using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<StockQuote>();
            return records.First();
        }
    }
}