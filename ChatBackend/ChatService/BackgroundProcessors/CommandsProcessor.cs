﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.queue;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ChatBackend.BackgroundProcessors
{
    public class CommandsProcessor: BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<CommandsProcessor> _logger;
        private readonly IQueueManager _queueManager;

        public CommandsProcessor(IConfiguration configuration, 
            ILogger<CommandsProcessor> logger,
            IQueueManager queueManager)
        {
            _configuration = configuration;
            _logger = logger;
            _queueManager = queueManager;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //conectar a la cola de comandos
            //si hay mensajes, procesarlos llamando a ProcessMessage
            //ProcessMessage toma el mensaje de la cola y lanza un 'procesador' en segundo plano
            //el 'procesador' llama a la api de stooq.com, parsea el resultado y lo pone en la cola de respuestas
            
            var activeTasks = new List<Task>();

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var cmdMessages = await _queueManager.DequeueCommands();

                    foreach (var cmdMessage in cmdMessages)
                    {
                        var quoteTask = new QuoteTask(cmdMessage, _queueManager);
                        activeTasks.Add(Task.Run(async () => await quoteTask.ResolveQuote(), stoppingToken));
                    }

                    // Remove any completed tasks.
                    activeTasks = activeTasks.Where(x => !x.IsCompleted).ToList();
                }
                catch (TaskCanceledException ex)
                {
                    var reason = ex.CancellationToken == stoppingToken
                        ? "Service is stopping."
                        : "Triggered by an unknown source.";
                    _logger.LogInformation(ex, $"Operation cancelled. {reason}");
                }
                catch (OperationCanceledException ex)
                {
                    var reason = ex.CancellationToken == stoppingToken
                        ? "Service is stopping."
                        : "Triggered by an unknown source.";
                    _logger.LogInformation(ex, $"Operation cancelled. {reason}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Unexpected exception while processing command.");
                }

                await Task.Delay(_configuration.GetValue("CommandProcessingMillis", 5000), stoppingToken);
            }
        }
    }
}