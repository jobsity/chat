# ChatRoom

**Exercise for Jobsity**

This application allows people to chat in a common room: all messages are delivered to all logged users.

Requests (other than **/login**) must include a valid jwt token to authenticate the user. 
The token is generated and returned as part of the login.

Messages can include a command to get stock quotes, with format

`/stock=[symbol]`

The system will process these commands asynchronously by calling stooq.com to get the information.
Once the results are returned by stooq.com, the system will post a new message to the room as
user 'quotebot'. These messages will not be persisted, they will be lost if the server goes down.

The server documents the API using OpenApi 3.0. An interactive UI will be displayed as root page
(https://localhost:5001).


## Requisites

* .net core 3.1. Download from here: https://dotnet.microsoft.com/download
* RabbitMQ server. Download from  https://www.rabbitmq.com/
* SqlServer localdb: instructions and download links here: https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/sql-server-express-localdb?view=sql-server-ver15
* NodeJS: https://nodejs.org/en/


## Execution

* Clone both backend and frontend repositories
  * this includes a LocalDB database with three users: user1, user2 and quotebot
    

* Configure RabbitMQ and SqlServer server access in `appsettings.json` 
  

* Backend: go to `chat/ChatBackend/ChatService` and execute `dotnet run`
  * open `https://localhost:5001` in your browser to access the swagger UI
  

* Frontend: go to `chat/chatfrontend/`
  * execute `npm install` to download all dependencies
  * execute `npm start` to start the development server 
  * open `localhost:3000` in your browser
  * login with `user1 / user123` or `user2 / user345`


## Endpoints:

### Messages
**GET /api/v1/Messages**

Returns last messages in the room (up to 50). Includes messages posted by users and 
responses to stock quotes commands.

**GET /api/v1/Messages/{id}**

Returns a single message given its id. For internal use.

**POST /api/v1/Messages**

Adds a message to the room. Message is sent in json format as body:

```
{
  "idOrigin": int,
  "nameOrigin": string,
  "content": string
}
```
* originId is the id of the user that sends the message.
* originName is the name of the user that sends the message.
* content is the message.


### Users
**POST /api/v1/login**

Allows the login of a user. Expects the user name and password as multipart/form-data

**username**: name of the user

**password**: password

**GET /api/v1/users**

Returns a list of current users.

**GET /api/v1/users/{id}**

Gets a single user given its internal id.

**POST /api/v1/users/{id}**

Adds a user to the system. User data is sent as json in the body of the request:
```
{
  "id": int,
  "username": string,
  "password": string,
  "email": string
}
```

* id: id of the user. Must be unique.
* username: name of the user.
* password: password (clear text)
* email: email address of the user

---

## Technical description

* The server is a .Net Core WebApi application
* There are two background processes that are launched at start and run every 5 seconds.
They manage a couple of queues to allow for detached processing of stock quote commands.
* When a command `/stock=[symbol]` is sent as a message, the api puts the message in a queue and returns.
Next time the background service for commands run, it will get the messages from the queue and
start a Task to query `stooq.com` for the symbol. Once a response is obtained, it is put
in another queue. 
* A second background process monitors that queue and take the responses and send them to an in-memory
list to be retrieved later.
* The endpoint `GET /messages` looks for at most 50 (configurable) messages in the database and 50
messages in the in-memory quotes storage. It merges both lists, sort by timestamp, and returns the
latest 50.
* Every endpoint save for `/login` needs a valid JWT token which is generated at login time and
sent back along with the userId and name. These will be used by the frontend to display who posted
each message. The user Id is not used currently but is passed along anyway for future expansion.
  