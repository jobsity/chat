SqlLocalDB create "ChatDB" -s
creates the server instance and starts it (-s)

Get info about the instance:
SqllocalDB.exe i ChatDB

Start the instance (if needed):
SqllocalDB.exe s ChatDB

Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;



To create the chat database, connect to the instance above and issue this

USE [master]
GO

/****** Object:  Database [chat]    Script Date: 10/5/2020 11:12:03 AM ******/
CREATE DATABASE [chat]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'chat', FILENAME = N'D:\archi\ernesto\prog\jobsity\chat\chat.mdf' , SIZE = 20480KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'chat_log', FILENAME = N'D:\archi\ernesto\prog\jobsity\chat\chat_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

USE [chat]
GO

/****** Object:  Table [dbo].[Messages]    Script Date: 10/5/2020 4:03:26 PM ******/
CREATE TABLE [dbo].[Messages](
	[Id] [int] NOT NULL IDENTITY,
	[Timestamp] [datetimeoffset] NOT NULL,
	[IdOrigin] [int] NULL,
	[Content] [nvarchar](2000) NULL,
	[nameOrigin] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 10/5/2020 4:03:33 PM ******/
CREATE TABLE [dbo].[Users](
	[Id] [int] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](250) NOT NULL,
	[Email] [nvarchar](250) NULL
 CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

